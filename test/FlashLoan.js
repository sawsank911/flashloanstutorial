const { expect } = require("chai");
const { ethers } = require ("hardhat");

const tokens = (n) => {
    return ethers.utils.parseUnits(n.toString(), "ether")
}

const ether = tokens

describe("FlashLoan", () => {

    let token, flashLoan, flashLoanReceiver;
    let deployer;
    let transaction;
    let totalTokenSupply = tokens(1000000);

    beforeEach(async () => {

        //Load Accounts
        let accounts = await ethers.getSigners()
        deployer = accounts[0]

        //Get Contracts
        const FlashLoan = await ethers.getContractFactory("FlashLoan");
        const FlashLoanReceiver = await ethers.getContractFactory("FlashLoanReceiver");
        const Token = await ethers.getContractFactory("Token");

        //Deploy Tokens
        token = await Token.deploy("SAW Lending Pool", "SALP", totalTokenSupply);

        //Deploy Flash Loan Pool
        flashLoan = await FlashLoan.deploy(token.address);

        // Deploy Flash Loan Receiver
        flashLoanReceiver = await FlashLoanReceiver.deploy(flashLoan.address)

        //Approve transaction
        transaction = await token.connect(deployer).approve(flashLoan.address, tokens(totalTokenSupply))
        await transaction.wait();

        //Deposit all tokens to loan pool
        transaction = await flashLoan.connect(deployer).depositTokens(totalTokenSupply)
        await transaction.wait()

    })

    describe("deployment", () => {

        it("loaded tokens to loan pool", async () => {
            expect(await token.balanceOf(flashLoan.address)).to.equal(totalTokenSupply)
        })
    })

    describe("loan transfers", () => {

        it("get loan from pool", async () => {
            let amount = tokens(100);
            transaction = await flashLoanReceiver.connect(deployer).executeFlashLoan(amount);
            await transaction.wait();

            await expect(transaction).to.emit(flashLoanReceiver, "LoanReceived")
                .withArgs(token.address, amount)
        })
    
    })
})