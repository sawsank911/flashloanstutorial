// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "./Token.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

interface IReceiver {
    function receiveTokens(address tokenAddress, uint256 amount) external;
}

contract FlashLoan is ReentrancyGuard {
    using SafeMath for uint256;
    Token public token;
    uint256 public poolBalance;

    constructor(address _tokenAddress) {
        token = Token(_tokenAddress);
    }

    function depositTokens(uint256 _amount) external nonReentrant {
        require(_amount > 0, "Amount must be greater than 0");
        token.transferFrom(msg.sender, address(this), _amount);
        poolBalance = poolBalance.add(_amount);
    }

    function flashLoan(uint256 _borrowAmount) external nonReentrant {
        // requirements
        require(_borrowAmount > 0, "Amount must be at least 1");
        // Balance before
        uint256 balanceBefore = token.balanceOf(address(this));
        require(_borrowAmount <= balanceBefore, "Amount exceeds pool balance");
        // Ensure if poolBalance and balanceBefore is same
        assert(poolBalance == balanceBefore);
        // Send tokens to receiver
        token.transfer(msg.sender, _borrowAmount);
        // Get paid back
        IReceiver(msg.sender).receiveTokens(address(token), _borrowAmount);
        // Check if paid back
        uint256 balanceAfter = token.balanceOf(address(this));
        require(balanceAfter >= balanceBefore, "Loan not paid back");
    }
}