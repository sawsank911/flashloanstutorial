// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "./FlashLoan.sol";
import "./Token.sol";

contract FlashLoanReceiver {
    FlashLoan private pool;
    address private owner;

    event LoanReceived(address _tokenAddress, uint256 _amount);

    constructor(address _poolAddress){
        pool = FlashLoan(_poolAddress);
        owner = msg.sender;
    }

    function receiveTokens(address _address, uint _amount) external {
        require(msg.sender == address(pool), "Only pool can call this function");
        require(Token(_address).balanceOf(address(this)) == _amount, "Failed to borrow amount from pool");
        emit LoanReceived(_address, _amount);
        require(Token(_address).transfer(msg.sender, _amount), "Failed to pay back.");
    }

    function executeFlashLoan(uint _amount) external {
        require(msg.sender == owner, "Only owner can execute the flash loan");
        pool.flashLoan(_amount);
    }
}